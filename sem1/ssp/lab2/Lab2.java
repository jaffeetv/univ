import java.util.Vector;

public class Lab2 {
    public static void main(String[] argv) {
        Vector<Double> input = parse_input(argv);
        int n = input.elementAt(input.size() - 1).intValue();
        input.removeElementAt(input.size()-1);

        task_1(input, n);
        double[] arr = task_2(new double[][] {{1,2}, {3,4}});
        // task_2 output
        for(int i = 0; i < arr.length; ++i) {
            System.out.println(arr[i]);
        } // task_2 end_output

        // task_3 output
        String key = "JAVA";
        String xor = task_3("Hello", key);
        System.out.println("crypt: " + xor);
        System.out.println("decrypt: " + task_3(xor, key));

        Vector<Vector<Double>> fvals = getFuncValues(-10.0, 20.0);
        Double max = 0.0;
        Double min = 0.0;
        for(int i = 0; i < fvals.size(); ++i) {
            if(fvals.elementAt(i).elementAt(1).doubleValue() > max) max = fvals.elementAt(i).elementAt(1).doubleValue();
            if(fvals.elementAt(i).elementAt(1).doubleValue() < min) min = fvals.elementAt(i).elementAt(1).doubleValue();
            System.out.format("%d\t\t%3.5e\t\t%3.5e\n", i, fvals.elementAt(i).elementAt(0).doubleValue(), fvals.elementAt(i).elementAt(1).doubleValue());
        }
        System.out.format(">%3.5e< : <%3.5e>\n", min, max);
    }
    public static void task_1(Vector<Double> v, int N) {
        for(int i = 0; i < N; ++i) {
            System.out.println(v.toString());
        }
    }
    public static double[] task_2(double[][] array) {
        int res_length = 0;
        for(int i = 0; i < array.length; ++i)
            res_length += array[i].length;
        double[] res = new double[res_length];

        int k = 0;
        for(int i = 0; i < array.length; ++i)
            for(int j = 0; j < array[i].length; j++)
                res[k++] = array[i][j];

        return res;
    }

    public static String task_3(String a, String b) {
        byte[] txt = a.getBytes();
		byte[] key = b.getBytes();
		byte[] res = new byte[a.length()];

		for (int i = 0; i < txt.length; i++) {
			res[i] = (byte) (txt[i] ^ key[i % key.length]);
		}
        return new String(res);
    }

    
    public static Double func(Double x) {
        Double res = (Math.pow(4.0, Math.pow(-(x-1), 2.0)) * Math.cos(2.0 * x));
        return res;
    }
    public static Vector<Vector<Double>> getFuncValues(Double a, Double b) {
        Vector<Vector<Double>> res = new Vector<Vector<Double>>();
        for(Double i = a; i < b; i += 0.1){
            Vector<Double> current = new Vector<Double>();
            current.addElement(i);
            current.addElement(func(i));
            res.addElement(current);
        }
        return res;
    }


    public static Vector<Double> parse_input(String[] argv) {
        Vector<Double> res = new Vector<Double>(argv.length);
        for(int i = 0; i < argv.length; ++i) {
            res.addElement(Double.parseDouble(argv[i]));
        }
        return res;
    }
}