#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

#include <experimental/filesystem>

#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/writer.h>


#define LAMBDA_PARAM 0.8
#define OJSON_FNAME "expdist.json"

using namespace rapidjson;
namespace fs = std::experimental::filesystem;

std::vector<double> exponent_distribution(std::vector<double> &quasi_nums) {
  std::vector<double> res;

  for (int i = 0; i < quasi_nums.size(); ++i) {
    res.push_back((-1.0 / LAMBDA_PARAM) * std::log(quasi_nums[i]));
  }
  return res;
}

int main(int argc, char **argv) {
  std::ifstream fin(argv[1]);
  IStreamWrapper isw(fin);

  Document json_param;

  json_param.ParseStream(isw);

  // read array of quasi numbers from json
  std::vector<double> quasinums;
  for (Value::ConstValueIterator itr = json_param.Begin();
       itr != json_param.End(); ++itr) {
    quasinums.push_back(itr->GetDouble());
  }

  auto expdist = exponent_distribution(quasinums);

  fs::path ojson_path{ fs::path{argv[1]}.remove_filename() };
  ojson_path += OJSON_FNAME;
  std::cout << ojson_path << std::endl;
  std::ofstream ofs(ojson_path);
  
  Value res;
  res.SetArray();
  Document od;
  for(auto &i : expdist) {
    res.PushBack(Value(i), od.GetAllocator());
  }


  OStreamWrapper osw(ofs);
  Writer<OStreamWrapper> writer(osw);
  res.Accept(writer);

  return 0;
}
