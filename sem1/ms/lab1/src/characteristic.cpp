#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <utility>
#include <vector>

#include <experimental/filesystem>

#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/writer.h>

#define RANGE_COUNT 5

struct Range {
  int min, max;
  std::vector<double> x;
  Range(int mn, int mx) : min(mn), max(mx) {}
  Range() {}

  void set_vals_into_range(std::vector<double> &vals) {
    for (auto &i : vals) {
      if (i < min || i >= max)
        continue;
      x.push_back(i);
    }
  }
};

struct FreqRange {
  typedef std::pair<Range, int> RIpair;
  std::vector<RIpair> freq_range;
  FreqRange(int min, int max, int rcount, std::vector<double> vals) {
    double range_length =
        static_cast<double>(max) / static_cast<double>(rcount);

    double min_curr = min;
    double max_curr = min + range_length;

    RIpair ripair = std::make_pair(Range(), int());
    for (int i = 0; i < rcount; ++i) {
      ripair.first.min = min_curr;
      ripair.first.max = max_curr;
      ripair.first.set_vals_into_range(vals);
      ripair.second = ripair.first.x.size();

      freq_range.push_back(ripair);
      min_curr = max_curr;
      max_curr += range_length;
      ripair.first.x.clear();
    }
  }
};

double math_expectation(std::vector<double> vals) {
  auto average = [&vals]() -> double {
    double res = 0;
    for (auto &i : vals)
      res += i;
    return res;
  };

  return average() / static_cast<double>(vals.size());
}

double math_dispersion(std::vector<double> vals) {
  double math_exp = math_expectation(vals);

  auto deviation = [&math_exp, vals]() -> double {
    double res = 0;
    for (auto &i : vals)
      res += std::pow(vals[i] - math_exp, 2.0);
    return res;
  };

  return deviation() / static_cast<double>(vals.size() - 1);
}

double avg_deviation(std::vector<double> vals) {
  double dispersion = math_dispersion(vals);

  return std::pow(dispersion / static_cast<double>(vals.size()), 0.5);
}

double probability_range_location(Range &rg) {
  return std::exp(-0.8 * rg.min) - std::exp(-0.8 * rg.max);
}

double pearson_criterion(std::vector<double> vals) {
  std::sort(vals.begin(), vals.end());
  int random_range_max = static_cast<int>(vals[vals.size() - 1]) + 1;
  int random_range_min = 0;

  FreqRange freqrange{random_range_min, random_range_max, RANGE_COUNT, vals};

  double pearson = 0;
  for (auto &i : freqrange.freq_range) {
    pearson +=
        std::pow(i.second - vals.size() * probability_range_location(i.first),
                 2) /
        vals.size() * probability_range_location(i.first);
  }

  return pearson;

  /* int fr_count = 0; */
  /* for(auto &i : freqrange.freq_range){ */
  /*   fr_count += i.second; */
  /*   for(auto &j : i.first.x){ */
  /*     std::cout << j << ";"; */
  /*   } */
  /*   std::cout << std::endl; */
  /* } */
  /* std::cout << vals.size() << ":" << fr_count << std::endl; */
}

using namespace rapidjson;
namespace fs = std::experimental::filesystem;

int main(int argc, char **argv) {
  std::ifstream fin(argv[1]);
  IStreamWrapper isw(fin);

  Document json_param;

  json_param.ParseStream(isw);

  // read array of quasi numbers from json
  std::vector<double> expdist;
  for (Value::ConstValueIterator itr = json_param.Begin();
       itr != json_param.End(); ++itr) {
    expdist.push_back(itr->GetDouble());
  }

  std::cout << "Математическое ожидание: " << math_expectation(expdist) << std::endl;
  std::cout << "Дисперсия: " << math_dispersion(expdist) << std::endl;
  std::cout << "Средне квадратическое отклонение: " << avg_deviation(expdist) << std::endl;
  std::cout << "Критерий Пирсона(4 степени своборы): "<< pearson_criterion(expdist) << std::endl;

  return 0;
}
