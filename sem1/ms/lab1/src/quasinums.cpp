#include <cmath>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <utility>
#include <vector>
#include <experimental/filesystem>

#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/writer.h>

#define OJSON_FNAME "quasinums.json"

template <typename InputType> struct RecourseMethodParam {
  InputType a0, a1, z0, z1, m;
  int ncount;
  RecourseMethodParam(InputType a0, InputType a1, InputType z0, InputType z1,
                      InputType m, int ncount)
      : a0(a0), a1(a1), z0(z0), z1(z1), m(m), ncount(ncount) {}
};

std::vector<double> recourse_method(RecourseMethodParam<int> rmp) {
  long long z_next, z_prev = rmp.z1, z_pprev = rmp.z0;
  long double x_curr = 0;
  std::vector<double> res;
  for(int i = 0; i < rmp.ncount; ++i) {
    z_next = rmp.a0 * z_pprev + rmp.a1 * z_prev; 
    x_curr = z_next % rmp.m;
    res.push_back(x_curr / static_cast<double>(rmp.m));

    z_pprev = z_prev;
    z_prev = z_next;
  }

  return res;
}

using namespace rapidjson;
namespace fs = std::experimental::filesystem;

int main(int argc, char **argv) {
  std::ifstream fin(argv[1]);
  IStreamWrapper isw(fin);

  Document json_param;

  json_param.ParseStream(isw);

  RecourseMethodParam<int> recourse_param(
      json_param["a0"].GetInt(), json_param["a1"].GetInt(),
      json_param["z0"].GetInt(), json_param["z1"].GetInt(),
      json_param["m"].GetInt(),  json_param["numbers_count"].GetInt());

  auto recourse_method_res = recourse_method(recourse_param);

  
  fs::path ojson_path{ fs::path{argv[1]}.remove_filename() };
  ojson_path += OJSON_FNAME;
  std::cout << ojson_path << std::endl;
  std::ofstream ofs(ojson_path);
  
  Value res;
  res.SetArray();
  Document od;
  for(auto &i : recourse_method_res) {
    res.PushBack(Value(i), od.GetAllocator());
  }


  OStreamWrapper osw(ofs);
  Writer<OStreamWrapper> writer(osw);
  res.Accept(writer);

  return 0;
}
