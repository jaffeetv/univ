#include "iostream"
#include "random"

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"





cv::Vec<uchar, 3> get_filtred_pixel(cv::Mat &mat, cv::Mat &filter, int x, int y) {
  typedef cv::Vec<uchar, 3> VecU3;
  #define mat(x,y) (mat.at<VecU3>(x,y))
  #define filter(x,y) (filter.at<VecU3>(x,y))
  // if( ((mat.size().width % 2) == 0) || mat.size().height != mat.size().width) return mat;
  VecU3 res;  

  res[0] = mat(x-1,y-1)[0] * filter(x-1,y-1)[0] + mat(x,y-1)[0] * filter(x,y-1)[0] + mat(x+1,y-1)[0] * filter(x+1,y-1)[0];
  res[1] = mat(x-1,y)[1] * filter(x-1,y)[1] + mat(x,y)[1] * filter(x,y)[1] + mat(x+1,y)[1] * filter(x+1,y)[1];
  res[2] = mat(x-1,y+1)[2] * filter(x-1,y+1)[2] + mat(x,y+1)[2] * filter(x,y+1)[2] + mat(x+1,y+1)[2] * filter(x+1,y+1)[2];
  
  //res[0] = filter.at<VecU4>(x-1,y-1)

  return res;
}

cv::Mat apply_filter(cv::Mat img, cv::Mat filter, uchar p[3]) {
  typedef cv::Vec<uchar, 3> VecU3;
  cv::Mat local_copy;
  img.copyTo(local_copy);

  VecU3 tmp_vecu3;
  int p1 = 120, p2 = 120, p3 = 120;
  for(auto i = 1ll; i < img.size().height - 1; ++i)
    for(auto j = 1ll; j < img.size().width - 1; ++j) {
      /* std::cout << i << " : " << j << std::endl; */
      tmp_vecu3 = get_filtred_pixel(local_copy, filter, i, j);
      if(tmp_vecu3[0] > p[0] && tmp_vecu3[1] > p[1] && tmp_vecu3[2] > p[2]) {
        local_copy.at<VecU3>(i,j)[0] = 255;
        local_copy.at<VecU3>(i,j)[1] = 255;
        local_copy.at<VecU3>(i,j)[2] = 255;
      } else {
        local_copy.at<VecU3>(i,j)[0] = 0;
        local_copy.at<VecU3>(i,j)[1] = 0;
        local_copy.at<VecU3>(i,j)[2] = 0;
      }
    }
  return local_copy;
}

int main(int argc, char **argv) {
  cv::Mat img = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);
  img.convertTo(img, CV_8UC3);
  uchar p[3] = {200, 200, 200};

  cv::Mat filter = (cv::Mat_<uchar>(3,3) << -1, -1, -1, -1, 8, -1, -1, -1, -1);

  cv::namedWindow("Filtred image", cv::WINDOW_AUTOSIZE);
  cv::Mat img_filtred;
  for(int i = 0; i < 20; ++i) {
    img_filtred = apply_filter(img, filter, p);
    cv::imshow("Filtred image", img_filtred);
    cv::waitKey(0);
    p[0] -= 10; p[1] -= 10; p[2] -= 10;
  }




  cv::destroyWindow("Filtred image");
  return 0;
}
