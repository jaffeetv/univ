#include "iostream"
#include "cmath"

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

typedef cv::Vec<uchar, 3> VecUc3;

void drawLine(cv::Mat &mat, cv::Point2i a, cv::Point2i b) {

  int xerr =0, yerr = 0;
  int dx = b.x - a.x, dy = b.y - a.y;

  int incX = (dx > 0) ? 1 : ((dx == 0)? 0 : -1);
  int incY = (dy > 0) ? 1 : ((dy == 0)? 0 : -1);
  
  dx = abs(dx); dy = abs(dy);

  int d = (dx > dy)? dx : dy;

  int x = a.x; int y = a.y;

  for(int i = 0; i < d; ++i) {
    xerr = xerr + dx;
    yerr = yerr + dy;

    if(xerr > d){
      xerr -= d;
      x += incX;
    }
    if(yerr > d){
      yerr -= d;
      y += incY;
    }

    mat.at<VecUc3>(x, y) = VecUc3{0, 0, 0};
  }
}

void drawCircle(cv::Mat &mat, cv::Point2i a, int radius) {
  int x, y, dxt;
  long r2, dst, t, s, e, ca, cd, indx;

  int xc = a.x; int yc = a.y;
  
  r2 = (long)radius * (long)radius;
  dst = 4*r2;
  dxt = (double)radius/1.414213562373;
  t = 0;
  s = -4*r2*(long)radius;
  e = (-s/2) - 3*r2;
  ca = -6*r2;
  cd = - 10*r2;
  x = 1;
  y = radius;

  mat.at<VecUc3>(xc, yc + radius) = VecUc3{0, 0, 0};
  mat.at<VecUc3>(xc, yc - radius) = VecUc3{0, 0, 0};
  mat.at<VecUc3>(xc + radius, yc) = VecUc3{0, 0, 0};
  mat.at<VecUc3>(xc - radius, yc) = VecUc3{0, 0, 0};

  for(indx = 1; indx <= dxt; ++indx, ++x) {
    if (e >= 0) e += t + ca;
    else {
      y--;
      e += t - s + cd;
      s += dst;
    }
    t -= dst;
    mat.at<VecUc3>(xc + x, yc + y) = VecUc3{0, 0, 0};
    mat.at<VecUc3>(xc + y, yc + x) = VecUc3{0, 0, 0};
    mat.at<VecUc3>(xc + y, yc - x) = VecUc3{0, 0, 0};
    mat.at<VecUc3>(xc + x, yc - y) = VecUc3{0, 0, 0};
    mat.at<VecUc3>(xc - x, yc - y) = VecUc3{0, 0, 0};
    mat.at<VecUc3>(xc - y, yc - x) = VecUc3{0, 0, 0};
    mat.at<VecUc3>(xc - y, yc + x) = VecUc3{0, 0, 0};
    mat.at<VecUc3>(xc - x, yc + y) = VecUc3{0, 0, 0};
  }
}

int main(int argc, char **argv) {
  cv::Mat img{480, 640, CV_8UC3, VecUc3{255, 255, 255}};

  drawLine(img, cv::Point2i(0,0), cv::Point2i(120, 10));
  drawCircle(img, cv::Point2i(240, 320), 50);

  cv::namedWindow("Lab4");
  cv::imshow("Lab4", img);
  cv::waitKey();
  return 0;
}
