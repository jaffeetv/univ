#! /usr/bin/bash
# Include header files into compilation database
# https://github.com/tomv564/LSP/issues/2

compdb -c compdb.complementers=headerdb -p . update 
compdb -c compdb.complementers=headerdb -p . list > compile_commands_full.json
mv compile_commands_full.json compile_commands.json
#rm headerdb.json
