# coding=utf-8

from faker import Faker
import sqlite3
import random

CLIENTS_COUNT = 100
ORDER_COUNT = 1000

fake = Faker('ru_RU')

# SQL CONNECTION
conn = sqlite3.connect("data.db")
c = conn.cursor()

c.execute('''CREATE TABLE clients(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        addr TEXT
        )''')
c.execute('''
        CREATE TABLE services(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT,
                price TEXT
                )
        ''')
c.execute('''
        CREATE TABLE orders(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                service_id INTEGER,
                client_id INTEGER,
                serial_number BLOB,
                date TEXT,
                FOREIGN KEY(service_id) REFERENCES services(id),
                FOREIGN KEY(client_id) REFERENCES clients(id)
                )
        ''')

# clients & services
for _ in range(0, CLIENTS_COUNT):
        profile = fake.simple_profile(sex=None)
        name = profile['name']
        addr = profile['address']
        c.execute('INSERT INTO clients(name, addr) VALUES ("{0}", "{1}");'.format(name, addr))

        service_name = fake.sentence(nb_words=2)
        price = fake.credit_card_security_code(card_type=None)
        c.execute('INSERT INTO services(name, price) VALUES("{0}", "{1}");'.format(service_name, price))

# orders
for _ in range(0, ORDER_COUNT):
        service_id = random.randint(0, CLIENTS_COUNT)
        client_id = random.randint(0, CLIENTS_COUNT)
        date = '{0} {1}'.format(fake.date(pattern="%Y-%m-%d", end_datetime=None), fake.time(pattern="%H:%M:%S", end_datetime=None))
        c.execute('INSERT INTO orders(service_id, client_id, date) VALUES("{0}", "{1}", "{2}")'.format(service_id, client_id, date))

conn.commit()
conn.close()

# c.execute("SELECT * FROM 'clients'")
# print(c.fetchone())

