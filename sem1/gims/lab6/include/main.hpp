#ifndef MAIN_HPP
#define MAIN_HPP

#include "QWidget"
#include "QSql"
#include "QSqlDatabase"
#include "QSqlTableModel"
#include "QFileDialog"
#include "QItemSelectionModel"
#include "QSqlRecord"
#include "QSqlField"
#include "QByteArray"
#include "QBuffer"
#include "QPixmap"
#include "QString"
#include "QImage"
#include "ui_main.h"

#include <exception>
#include <iostream>

class Service;


class Service : public QMainWindow, public Ui::MainWindow {
   Q_OBJECT
public:
    Service(QString dbpath = 0, QWidget *pwd = 0) : QMainWindow(pwd) {
        setupUi(this);
        if(dbpath == 0)
          std::runtime_error("Invalid argv[1] -- see uses\n");
        db.setDatabaseName(dbpath);
        if(!db.open())
          std::runtime_error("Cannot open db\n");


        model.setTable("orders");
        model.select();
        model.setEditStrategy(QSqlTableModel::OnRowChange);

        tableView->setModel(&model);
        connect(pushButton, SIGNAL(released()), this, SLOT(addImg2Db()));
        connect(tableView, SIGNAL(clicked(QModelIndex)), this, SLOT(imgUpdate(QModelIndex)));
    }
private:
    int getSelectedRow() {
        QItemSelectionModel *selection = tableView->selectionModel();
        if(!selection)
          std::runtime_error("selectionModel() is null");

        return selection->selectedIndexes().at(0).row();
    }
    void updateSelectedRow() {
        QItemSelectionModel *selection = tableView->selectionModel();
        tableView->update(selection->selectedIndexes().at(0));
    }
    QByteArray qImg2qByteArray(QImage &img) {
        QByteArray arr;
        QBuffer buffer(&arr);
        buffer.open(QIODevice::WriteOnly);
        img.save(&buffer, "PNG");
        return arr;
    }

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QSqlTableModel model = QSqlTableModel(this, db);
    QImage imglabel;

public slots:
    void addImg2Db(){
        QString fname = QFileDialog::getOpenFileName(this, tr("Open File"), "/home");
        QImage img{fname};
        int nrow = getSelectedRow();
        QSqlRecord row_record = model.record(nrow);
        row_record.setValue("serial_number", qImg2qByteArray(img));
        if(!model.setRecord(nrow, row_record))
          std::runtime_error("Cannot set record into db\n");
        updateSelectedRow();
    }

    void imgUpdate(QModelIndex index) {
        int nrow = index.row();
        QPixmap pix;
        pix.loadFromData(model.record(nrow).value("serial_number").toByteArray(), "PNG");
        label->setPixmap(pix.scaledToWidth(400));
    }
};

#endif // MAIN_HPP
