cmake_minimum_required(VERSION 3.7)
project(lab6 CXX)
SET(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)



# qt5_wrap_cpp(${})

SET(${PROJECT_NAME}_SRC
  src/main.cpp
)

SET(${PROJECT_NAME}_HEADERS
  include/main.hpp
)

SET(${PROJECT_NAME}_UI
  include/main.ui
  #include/colororder_dialog.ui
)

find_package(Qt5 COMPONENTS Core Widgets Sql REQUIRED)


include_directories(include)

qt5_wrap_cpp(${PROJECT_NAME}_MOC_HEADERS ${${PROJECT_NAME}_HEADERS})
qt5_wrap_ui(${PROJECT_NAME}_UI_HEADERS ${${PROJECT_NAME}_UI})

add_executable(${PROJECT_NAME} 
  ${${PROJECT_NAME}_SRC}  
  ${${PROJECT_NAME}_UI}
  ${${PROJECT_NAME}_UI_HEADERS}
  ${${PROJECT_NAME}_MOC_HEADERS}
)
target_link_libraries(${PROJECT_NAME}
  Qt5::Core Qt5::Widgets Qt5::Sql
)
set_target_properties(${PROJECT_NAME} PROPERTIES CXX_STANDARD 17)


# generate compilation db with header files
# copy compdb into project root for compatibility with vim
add_custom_target(cc COMMAND 
  "${PROJECT_SOURCE_DIR}/scripts/headercompdb.sh" "1>" "/dev/null" "&&" 
  "cp" "${PROJECT_BINARY_DIR}/compile_commands.json" "${PROJECT_SOURCE_DIR}" 
)
