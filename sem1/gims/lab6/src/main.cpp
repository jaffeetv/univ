#include <iostream>

#include <QApplication>
#include <QtWidgets>

#include "main.hpp"

using namespace std;


int main(int argc, char **argv)
{
  QApplication app(argc, argv);
  Service service{argv[1]};
  service.show();
  return app.exec();
}
