#ifndef SCENE3D_H
#define SCENE3D_H
#include <QGLWidget>
#include <QtOpenGL>
#include <QTimer>

#include <utility>

class Rectangle;
typedef std::map<int, Rectangle> Circles;

struct Shape {
  virtual void draw() = 0;
  virtual void set(int dx, int dy, int xsize, int ysize) = 0;
  virtual bool is_movable() = 0;
  int cdx, cdy;
  int x_size, y_size;
  bool movable;
};

class Rectangle : public Shape {
public:
  Rectangle(int dx, int dy, int xsize, int ysize) {
    cdx = dx;
    cdy = dy;
    x_size = xsize;
    y_size = ysize;
    movable = true;
    r = 1; g = 0; b = 0;
  }
  Rectangle() {
    r = 0; g = 1; b = 0;
    movable = false;
  }
  void set(int dx, int dy, int xsize, int ysize) {
    cdx = dx;
    cdy = dy;
    x_size = xsize;
    y_size = ysize;
  }
  bool is_movable() {return movable;}
  void draw() {
    glBegin(GL_POLYGON);
    glColor3f(r,g,b);// Цвет выделенной области
    // Координаты выделенной области
    glVertex2f(cdx, cdy);
    glVertex2f(cdx+x_size, cdy);
    glVertex2f(cdx+x_size, cdy+y_size);
    glVertex2f(cdx, cdy+y_size);
    glEnd();
  }

  double r, g, b;
};



class MainWindow : public QGLWidget
{
Q_OBJECT
public:
  MainWindow(QWidget *parent = 0);


  Rectangle cylinders[3];
  Circles circles;
protected:
  //int geese_size; // Размер гуся))
  //int point; // набранные очки
  int gdx, gdy; // Координаты объектов (гусей)
  int cax, cay, cbx, cby; // Координаты курсора
  int wax ,way; // Размеры окна
  //bool singling; // Для выделение области
  void self_cursor(); // метод для рисования своего курсора
  void initializeGL(); // Метод для инициализирования opengl
  void resizeGL(int nWidth, int nHeight); // Метод вызываемый после каждого изменения размера окна
  void paintGL(); // Метод для вывода изображения на экран
  void keyPressEvent(QKeyEvent *ke); // Для перехвата нажатия клавиш на клавиатуре
  void mouseMoveEvent(QMouseEvent *me); // Метод реагирует на перемещение указателя, но по умолчанию setMouseTracking(false)
  void mousePressEvent(QMouseEvent *me); // Реагирует на нажатие кнопок мыши
  void mouseReleaseEvent(QMouseEvent *me); // Метод реагирует на "отжатие" кнопки мыши
  //void singling_lb(); // Рисуем рамку выделенной области
  //void geese(); // Рисуем объекты (будущих гусей :) )

  bool moving;
};


#endif // SCENE3D_H
