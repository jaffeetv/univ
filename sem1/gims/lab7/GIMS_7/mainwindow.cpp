#include "mainwindow.h"
#include <QDebug>
#include <iostream>
#include <QSound>

bool isCursorIntoShape(QMouseEvent *me, Shape &shape);

MainWindow::MainWindow(QWidget* parent) : QGLWidget(parent)
{
  wax=500; way=500; // начальный размер окна
  setFormat(QGLFormat(QGL::DoubleBuffer)); // Двойная буферизация
  glDepthFunc(GL_LEQUAL); // Буфер глубины

  cylinders[0].set(100, 80, 10, 400);
  cylinders[1].set(250, 80, 10, 400);
  cylinders[2].set(400, 80, 10, 400);

  circles = {{0, Rectangle(cylinders[0].cdx - 55, cylinders[0].cdy + cylinders[0].y_size-20, 120, 20)},
             {1, Rectangle(cylinders[0].cdx - 45, cylinders[0].cdy + cylinders[0].y_size - 43, 100, 20)},
             {2, Rectangle(cylinders[0].cdx - 35, cylinders[0].cdy + cylinders[0].y_size - 66, 80, 20)},
             {3, Rectangle(cylinders[0].cdx - 25, cylinders[0].cdy + cylinders[0].y_size - 89, 60, 20)},
             {4, Rectangle(cylinders[0].cdx - 15, cylinders[0].cdy + cylinders[0].y_size - 112, 40, 20)}
            };
  QSound::play("/home/jaffee/proj/univ/sem1/gims/lab7/GIMS_7/coolest_sound.wav");
}
void MainWindow::initializeGL()
{
  qglClearColor(Qt::white); // Черный цвет фона
}
void MainWindow::resizeGL(int nWidth, int nHeight)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, (GLint)nWidth, (GLint)nHeight);
  wax=nWidth;
  way=nHeight;
}
void drawCilinders(Rectangle *cylinders, int size){
  for(int i = 0; i < size; ++i) {
      cylinders[i].draw();
    }
}
void drawCircles(Circles &circles) {
  for(auto &i : circles) {
      i.second.draw();
    }
}
void MainWindow::paintGL()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // чистим буфер изображения и буфер глубины
  glMatrixMode(GL_PROJECTION); // устанавливаем матрицу
  glLoadIdentity(); // загружаем матрицу
  glOrtho(0,wax,way,0,1,0); // подготавливаем плоскости для матрицы
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  qglColor(Qt::white);
  //renderText(10, 10 , 0, QString::fromUtf8("Вы набрали %1 очков:").arg(point),
  //QFont());

  drawCilinders(cylinders, 3);
  drawCircles(circles);

  self_cursor(); // Загружаем наш курсор
  swapBuffers();
}
void MainWindow::keyPressEvent(QKeyEvent *ke)
{
  switch (ke->key())
  {
    case Qt::Key_Space:
  break;
  }
    updateGL();
  }
void MainWindow::mouseMoveEvent(QMouseEvent *me)
{
  // Получаем координаты курсора
  cax=me->x();
  cay=me->y();
  updateGL();
}

void refreshHeights(Circles &circles, Rectangle &cylinder) {
  int k = 20;
  for(auto &i : circles) {
      i.second.set(i.second.cdx, cylinder.cdy + cylinder.y_size - k, i.second.x_size, i.second.y_size);
      k+= 23;
    }
}

void MainWindow::mousePressEvent(QMouseEvent *me)
{
  updateGL();
}
bool isCursorIntoShape(QMouseEvent *me, Shape &shape) {
  if((me->x() >= shape.cdx && me->x() <= shape.cdx + shape.x_size)
     && ((me->y() >= shape.cdy && me->y() <= shape.cdy + shape.y_size)))
     return true;
  return false;
}
void MainWindow::mouseReleaseEvent(QMouseEvent *me)
{
  for(auto &i : circles) {
      if(me->button() == Qt::LeftButton && isCursorIntoShape(me, i.second)){
//          int x_diff = me->x() - cilinder.cdx;
//          int y_diff = me->y() - cilinder.cdy;
//          cilinder.set(me->x()-x_diff, me->y() - y_diff, cilinder.x_size, cilinder.y_size);

          // for example
          qDebug() << i.first << "\n";
          circles.erase(i.first);


        }
      refreshHeights(circles, cylinders[0]);
    }
  updateGL();
}
void MainWindow::self_cursor()
{
  glBegin(GL_POLYGON);
  glColor3f(1,0,0);// Цвет курсора
  // Координаты курсора
  glVertex2f(cax, cay);
  glVertex2f(cax+20, cay+20);
  glVertex2f(cax+8, cay+20);
  glVertex2f(cax, cay+30);
  glEnd();
}
