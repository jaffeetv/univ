#ifndef TOWER_HPP
#define TOWER_HPP

#include <GLFW/glfw3.h>
#include <string>
#include <stdexcept>

using std::exception;
using std::runtime_error;
using std::string;



struct GlfwWindowsParam {
  GlfwWindowsParam(int width, int height, string name, GLFWmonitor *monitor = NULL, GLFWwindow *share = NULL)
    : width(width), height(height), name(name), monitor(monitor), share(share) {}
  GlfwWindowsParam() {}
  int width = 640;
  int height = 480;
  string name = "Windows";
  GLFWmonitor *monitor = NULL;
  GLFWwindow *share = NULL;
};

class Tower {
public:
  Tower() {
    if(!glfwInit()){
      glfwTerminate();
      throw runtime_error("glfwInit() NULL\n");
      }
    window = glfwCreateWindow(windows_param.width, windows_param.height, windows_param.name.c_str(), windows_param.monitor, windows_param.share);
    if(!window){
      glfwTerminate();
      throw runtime_error("glfwCreateWindow() NULL\n");
      }
    glfwMakeContextCurrent(window);
  }
  void run();
  ~Tower() {
    if(window) {
        glfwTerminate();
      }
  }
private:
  GLFWwindow *window;
  GlfwWindowsParam windows_param = GlfwWindowsParam(640, 480, "Tower of Hanoi", NULL, NULL);
};

#endif // TOWER_HPP
