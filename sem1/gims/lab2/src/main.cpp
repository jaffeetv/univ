#include <iostream>

#include <QApplication>
#include <QtWidgets>
#include <QString>

#include "main.hpp"

int main(int argc, char **argv) {
  QApplication app(argc, argv);
  AddressbookMain addr(argv[1]);
  addr.show();
  return app.exec();
}
