#ifndef MAIN_HPP
#define MAIN_HPP

#include "QApplication"
#include "QtWidgets"
#include "QDialog"
#include "QString"

#include "QtSql"
#include "QSqlTableModel"
#include "QSqlDatabase"
#include "QTableView"

#include "ui_main.h"
#include "ui_dialog.h"


class AddressbookMain;



class AddressbookMain : public QMainWindow, public Ui::MainWindow {
  Q_OBJECT
  QDialog editDialog;

  Ui::editDialog ui_dialog;

public:
  QSqlDatabase m_db;
  QTableView view;
  QSqlTableModel *model;



  AddressbookMain(QString path, QWidget *pwd = 0) : QMainWindow(pwd) { 
    setupUi(this);

    auto dbconnection = [&]() -> void {
      m_db = QSqlDatabase::addDatabase("QSQLITE");
      m_db.setDatabaseName(path);

      if (m_db.open())
      {
        qDebug() << "Error: connection with database fail";
      }
      else
      {
        qDebug() << "Database: connection ok";
      }
    };

    ui_dialog.setupUi(&editDialog);

    dbconnection();
    model = new QSqlTableModel(0, m_db);
    model->setTable("address_book");
    model->select();
    model->setEditStrategy(QSqlTableModel::OnFieldChange);

    tableView->setModel(model);
    /* view.show(); */

    connect(this->pushButton, SIGNAL(clicked()), this, SLOT(openDialog()));
  }

public slots:
  void openDialog() {
    editDialog.show();
  }

};


#endif // MAIN_HPP
