#ifndef MAIN_HPP
#define MAIN_HPP

#include "QApplication"
#include "QtWidgets"
#include "QFileDialog"
#include "QString"
#include "QImage"
#include "QPixmap"
#include "QDialog"
#include "QVector"
#include "QRgb"
#include "QColor"

#include <memory>
#include <iostream>

#include "ui_main.h"
#include "ui_colororder_dialog.h"

class Label_test : public QMainWindow {
  Q_OBJECT
public:
  Label_test(QWidget *parent = 0) : QMainWindow(parent), ui(new Ui::Form), color_order_ui(new Ui::Dialog) {
    ui->setupUi(this);
    color_order_ui->setupUi(&color_order);
    /* ui->label->setPixmap() */

  connect(ui->actionopen, SIGNAL(triggered()), this, SLOT(readBmpFile()));
  connect(ui->actionchannel_swapping, SIGNAL(triggered()), &color_order, SLOT(show()));
  connect(this, SIGNAL(imageChanged()), this, SLOT(redrawImage()));
  connect(color_order_ui->buttonBox, SIGNAL(accepted()), this, SLOT(colorSwapp()));
  }
signals:
  void imageChanged();
public slots:
  void readBmpFile() {
    QString fname = QFileDialog::getOpenFileName(this, tr("Open File"), "/home");
    mQImage.reset(new QImage(fname));
    emit imageChanged();
  }
  void redrawImage() {
    ui->label->setPixmap(QPixmap::fromImage(*mQImage));
  }
  void colorSwapp() {
    int order[3];
    for(int i = 0; i < mQImage->width(); ++i) {
      for(int j = 0; j < mQImage->height(); j++) {
        QColor cur_color = mQImage->pixelColor(i, j);
        order[color_order_ui->lineEdit->displayText().toInt()] = cur_color.red();
        order[color_order_ui->lineEdit_2->displayText().toInt()] = cur_color.green();
        order[color_order_ui->lineEdit_3->displayText().toInt()] = cur_color.blue();
        cur_color = QColor::fromRgb(order[0], order[1], order[2]).rgb();
        mQImage->setPixelColor(i,j, cur_color);
      }
    }
    emit imageChanged();
  }

private:
  std::unique_ptr<Ui::Form> ui;
  std::unique_ptr<Ui::Dialog> color_order_ui;

  std::shared_ptr<QImage> mQImage;
  QDialog color_order;

};

#endif // MAIN_HPP
