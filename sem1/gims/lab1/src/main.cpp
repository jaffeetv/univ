#include "iostream"
#include "random"

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

cv::Vec<uchar, 3> get_noised_pixel(cv::Vec<uchar, 3> p) {
  cv::Vec<uchar, 3> res{p};
  std::random_device rd;
  uchar color_255 = rd() % 3;

  res[color_255] = 255;

  return res;
}

cv::Mat noise_generation(cv::Mat img, int percent) {
  if (img.empty())
    return img;

  long long noise_pixel_count =
      img.size().height * img.size().width / 100 * percent;
  std::random_device rd;

  cv::Vec<uchar, 3> noised_pixel;
  for (auto i = 0ll; i < noise_pixel_count; ++i) {
    int x = rd() % img.size().height;
    int y = rd() % img.size().width;
    noised_pixel = get_noised_pixel(img.at<decltype(noised_pixel)>(x, y));
    img.at<decltype(noised_pixel)>(x, y) = noised_pixel;
  }

  return img;
}

cv::Vec<uchar, 3> get_filtred_pixel(cv::Mat &mat, cv::Mat &filter, int x, int y) {
  typedef cv::Vec<uchar, 3> VecU4;
  // if( ((mat.size().width % 2) == 0) || mat.size().height != mat.size().width) return mat;
  VecU4 res;  
  res[0] = 1.0/9.0 * static_cast<long double>(mat.at<VecU4>(x-1, y-1)[0] + mat.at<VecU4>(x, y-1)[0] + mat.at<VecU4>(x+1, y-1)[0] + 
           mat.at<VecU4>(x-1, y)[0] + mat.at<VecU4>(x, y)[0] + mat.at<VecU4>(x+1, y)[0] +
           mat.at<VecU4>(x-1, y+1)[0] + mat.at<VecU4>(x, y+1)[0] + mat.at<VecU4>(x+1, y+1)[0]);
  res[1] = 1.0/9.0 * static_cast<long double>(mat.at<VecU4>(x-1, y-1)[1] + mat.at<VecU4>(x, y-1)[1] + mat.at<VecU4>(x+1, y-1)[1] + 
          mat.at<VecU4>(x-1, y)[1] + mat.at<VecU4>(x, y)[1] + mat.at<VecU4>(x+1, y)[1] +
          mat.at<VecU4>(x-1, y+1)[1] + mat.at<VecU4>(x, y+1)[1] + mat.at<VecU4>(x+1, y+1)[1]);
  res[2] = 1.0/9.0 * static_cast<long double>(mat.at<VecU4>(x-1, y-1)[2] + mat.at<VecU4>(x, y-1)[2] + mat.at<VecU4>(x+1, y-1)[2] + 
          mat.at<VecU4>(x-1, y)[2] + mat.at<VecU4>(x, y)[2] + mat.at<VecU4>(x+1, y)[2] +
          mat.at<VecU4>(x-1, y+1)[2] + mat.at<VecU4>(x, y+1)[2] + mat.at<VecU4>(x+1, y+1)[2]);
  /* res[3] = 1.0/9.0 * static_cast<long double>(mat.at<VecU4>(x-1, y-1)[3] + mat.at<VecU4>(x, y-1)[3] + mat.at<VecU4>(x+1, y-1)[3] + */ 
  /*         mat.at<VecU4>(x-1, y)[3] + mat.at<VecU4>(x, y)[3] + mat.at<VecU4>(x+1, y)[3] + */
  /*         mat.at<VecU4>(x-1, y+1)[3] + mat.at<VecU4>(x, y+1)[3] + mat.at<VecU4>(x+1, y+1)[3]); */

  return res;
}

cv::Mat apply_filter(cv::Mat img, cv::Mat filter) {
  typedef cv::Vec<uchar, 3> VecU4;
  cv::Mat local_copy;
  img.copyTo(local_copy);
  for(auto i = 1ll; i < img.size().height - 1; ++i)
    for(auto j = 1ll; j < img.size().width - 1; ++j) {
      /* std::cout << i << " : " << j << std::endl; */
      local_copy.at<VecU4>(i,j) = get_filtred_pixel(local_copy, filter, i, j);
      
    }
  return local_copy;
}

int main(int argc, char **argv) {
  cv::Mat img = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);
  img.convertTo(img, CV_8UC3);
  cv::Mat filter = cv::Mat::zeros(3,3, CV_8U);

  auto img_noised = noise_generation(img, std::stoi(argv[2]));
  auto img_restored = apply_filter(img_noised, filter);

  cv::namedWindow("Noised image", cv::WINDOW_AUTOSIZE);
  cv::namedWindow("Filtred image", cv::WINDOW_AUTOSIZE);

  cv::imshow("Noised image", img);
  cv::imshow("Filtred image", img_restored);

  cv::waitKey(0);

  cv::destroyWindow("Noised image");
  cv::destroyWindow("Filtred image");
  return 0;
}
