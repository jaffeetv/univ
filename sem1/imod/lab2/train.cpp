#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include "train_common.hpp"

#include "rang.hpp"
#include <armadillo>
#include <nlohmann/json.hpp>

#include "matplotlibcpp.h"

namespace plt = matplotlibcpp;
using json = nlohmann::json;
using arma::mat;
using std::string;
using std::vector;
using std::pair;


pair<vector<double>, vector<double>> get_prediction(Train &train, int x) {
  mat img{train.train_param.n, 1, arma::fill::zeros};
  /* mat train_func_result{1, train.train_param.train_size + x, fill::zeros}; */
  /* for(int i = 0; i < train.train_param.train_size; ++i) { */
  /*   train_func_result(0, i) = train.train_func_result(0,i); */
  /* } */
  
  train.train_func_result.resize(1, train.train_param.train_size + x);

  vector<double> res;
  vector<double> pred;
  double cur_output = 0;
  double rec_output = 0;
  
  // 0.1 -- step !!!
  double delta = train.train_param.train_size * 0.1;
  for (int i = 0; i < x; ++i) {
    img = get_current_input_img(
        train, img, (int)train.train_param.train_size - (int)train.train_param.n + i);
    cur_output = get_current_output(train.weight, img, train.treshold);
    
    train.train_func_result(0, train.train_param.train_size + i) = cur_output;
    pred.push_back(train_func(train.train_param, delta));

    delta += 0.1;
  }

  for (auto i : train.train_func_result) {
    res.push_back(i);
  }

  return std::make_pair(res, pred);
}

int main(int argc, char **argv) {
  std::ifstream fin(argv[1]);
  TrainParam train_param{train_param_parse(fin)};

  Train train{train_param};

  /* std::cout << train.train_func_result << std::endl; */

  train.start();
  
  auto train_res_pair = get_prediction(train, 15);

  int train_size = int(train_param.train_size);
  std::cout << "Prediction\tRequired\tE" << std::endl;
  for(int i = 0; i < 15; ++i){
    std::cout << i + train_size + 1  
              << ") " << train_res_pair.first[i+train_size]
              << "\t" << train_res_pair.second[i]
              << "\t" << train_res_pair.first[i+train_size] - train_res_pair.second[i]
              << std::endl;
  }

  plt::plot(train_res_pair.first);
  plt::show();

}
