#ifndef TRAIN_COMMON_HPP
#define TRAIN_COMMON_HPP
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

#include "rang.hpp"
#include <armadillo>
#include <nlohmann/json.hpp>

using json = nlohmann::json;
using namespace arma;
using std::string;

struct TrainParam;
struct Train;

double train_func(TrainParam &train_param, int x);
double get_current_deviation(double result, double etalon);
double get_current_output(mat &weights, mat &img, mat &treshold);
double get_current_required_output(Train &train, mat &input_img, int image_n);
mat get_new_weights(Train &train, mat &weights, mat &input_img, double output, double output_required);
mat get_new_treshold(Train &train, mat &treshold, double output, double output_required);
mat &get_current_input_img(Train &train, mat &input_img, int x);


struct TrainParam {
  double a, b, d, n, train_size;
  double deviation;
  double train_speed;
  string train_func;

  TrainParam(const nlohmann::json &json_train_param) {
    a = json_train_param["a"];
    b = json_train_param["b"];
    d = json_train_param["d"];
    n = json_train_param["n"];
    train_size = json_train_param["train_size"];
    train_func = json_train_param["train_func"];
    deviation = json_train_param["deviation"];
    train_speed = json_train_param["train_speed"];
  };
  TrainParam(double param_a = 0, double param_b = 0, double param_d = 0,
             double param_n = 0, double param_train_size = 0,
             string param_train_func = "", double param_deviation = 0,
             double param_train_speed = 0) {
    a = param_a;
    b = param_b;
    d = param_d;
    n = param_n;
    train_size = param_train_size;
    train_func = param_train_func;
    deviation = param_deviation;
    train_speed = param_train_speed;
  }
};
double train_func(TrainParam &train_param, double x);
struct Train {
  TrainParam train_param;
  mat train_func_result;
  mat weight;
  mat treshold;
  mat input_img;
  mat output_img;
  long double sum_deviation;

  int iter_count;

  Train(TrainParam &train_param)
      : train_param(train_param), treshold{1, 1, fill::randu},
        weight{train_param.n, 1, fill::randu},
        train_func_result{1, train_param.train_size, fill::zeros},
        input_img{train_param.n, 1, fill::zeros},
        output_img{train_param.n, 1, fill::randu},
        iter_count{0} {
    double delta = 0;
    for (int i = 0; i < train_param.train_size; ++i) {
      train_func_result(0, i) = train_func(train_param, delta);
      delta += 0.1;
    }
  }
  void start() {
    mat new_weight{train_param.n, 1, fill::randu};
    mat new_treshold{1, 1, fill::randu};
    double output = 0;
    double output_required = 0;
    int image_count = train_param.train_size - train_param.n;

    auto sum_img = [&]() -> double {
      double sum = 0;
      for(int i = 0; i < 3; ++i) {
        sum += input_img(i, 0);
      }
      return std::pow(sum, 2.0);
    };

    do {
      sum_deviation = 0.0;
      iter_count += 1;
      for (int i = 0; i < image_count; ++i) {
        weight = new_weight;
        treshold = new_treshold;
        input_img = get_current_input_img(*this, input_img, i);
        output = get_current_output(weight, input_img, treshold);
        output_required = get_current_required_output(*this, input_img, i);

        /* //recalculatin weight & treshold */
        new_weight = get_new_weights(*this, weight, input_img, output, output_required);
        new_treshold = get_new_treshold(*this, treshold, output, output_required);

        sum_deviation += get_current_deviation(output, output_required);
        
        train_param.train_speed = 1.0 / (1.0 + sum_img());
      }

      /* std::cout << iter_count << ") "<< sum_deviation << std::endl; */
    } while (sum_deviation > train_param.deviation);
    std::cout << "iter_cout: " << iter_count << std::endl;
  }
  friend double train_func(TrainParam &train_param, double x);
  friend double get_current_deviation(double result, double etalon);
  friend double get_current_output(mat &weights, mat &img, mat &treshold);
  friend double get_current_required_output(Train &train, mat &input_img, int image_n);
  friend mat get_new_weights(Train &train, mat &weights, mat &input_img, double output, double output_required);
  friend mat get_new_treshold(Train &train, mat &treshold, double output, double output_required);
  friend mat &get_current_input_img(Train &train, mat &input_img, int x);
};

nlohmann::json train_param_parse(std::istream &param_file) {
  json res;
  try {
    res << param_file;
  } catch (json::parse_error &err) {
    string name{"json_param_parse"};
    std::cerr << rang::fg::red << err.what() << rang::style::reset << std::endl;
  }
  return res;
}

double train_func(TrainParam &train_param, double x) {
  return train_param.a * std::sin(train_param.b * x) + train_param.d;
}
double get_current_deviation(double result, double etalon) {
  return std::pow((result - etalon), 2) * 0.5;
}

// input_img: [i, j] where i -- rows;
mat &get_current_input_img(Train &train, mat &input_img, int x) {
  for (int i = 0; i < train.train_param.n; ++i) {
    input_img(i, 0) = train.train_func_result(0, x + i);
  }
  return input_img;
}

double get_current_output(mat &weights, mat &img, mat &treshold) {
  return (weights.t() * img - treshold)[0];
}

double get_current_required_output(Train &train, mat &input_img, int image_n) {
  return get_current_input_img(train, input_img, image_n + 1)[train.train_param.n - 1];
}


mat get_new_weights(Train &train, mat &weights, mat &input_img, double output, double output_required) {
  return weights - (train.train_param.train_speed*(output - output_required) * input_img);
}

mat get_new_treshold(Train &train, mat &treshold, double output, double output_required) {
  return treshold + (train.train_param.train_speed*(output - output_required));
}

#endif // TRAIN_COMMON_HPP
