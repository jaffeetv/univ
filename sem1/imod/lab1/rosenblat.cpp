#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>

#define ARMA_DONT_USE_WRAPPER
#define ARMA_USE_BLAS
#define ARMA_DONT_USE_OPENMP
#define ARMA_OPENMP_THREADS 1
#include <armadillo>

using namespace arma;
using std::cout, std::cin, std::endl;
using std::abs;

double activate_func(double summ) { return summ > 0.0 ? 1. : 0.; }

double find_sum(mat &input, mat &weight, mat &threshold) {
  mat temp_res = weight.t() * input - threshold;
  return temp_res[0];
}

mat find_weight(mat &input_layer, mat &required_res, mat &threshold) {
  mat weight(2, 1, fill::zeros);
  double alpha = 0.001;

  bool succ[4] = {false, false, false, false};
  auto succ_check = [&]() -> bool {
    return ((succ[0] && succ[1]) && (succ[2] && succ[3]));
  };
  while (!succ_check()) {
    for (uword i = 0; i < required_res.n_cols; ++i) {
      mat curr_input = input_layer.submat(i, 0, i, 1).t();
      double curr_res = activate_func(find_sum(curr_input, weight, threshold));
      /* cout << i << ") i:" << curr_input.t() << "; w:" << weight.t() << "; s:"
       * << curr_res << endl; */
      if (curr_res == required_res(i)) {
        succ[i] = true;
      } else {
        succ[i] = false;
        // weight calcutating start
        mat tmp(1, 1);
        tmp(0, 0) = (curr_res - required_res(i)) * alpha;
        mat delta_weight = curr_input * tmp;
        weight = weight - delta_weight;
        // weight calculating end

        // threshold calculating start
        threshold = threshold + tmp;
        // threshold calculating end
      }
    }
  }
  return weight;
}

int main(int argc, char **argv) {
  mat input_layer({{0, 0}, {0, 1}, {1, 0}, {1, 1}});
  mat required_res({0, 0, 0, 1});
  mat threshold(1, 1, fill::zeros);

  mat weight = find_weight(input_layer, required_res, threshold);

  char *end;
  mat user_input(2,1);
  user_input(0,0) = strtod(argv[1], &end);
  user_input(1,0) = strtod(argv[2], &end);
  cout << activate_func(find_sum(user_input, weight, threshold)) << endl;


  return 0;
}

